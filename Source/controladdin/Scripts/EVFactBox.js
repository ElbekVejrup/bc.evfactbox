function EVFactBox(base64pdf, options) {

    let mainbody = window.parent.document.body;
    if (mainbody !== null) {
        // Hide self. - find by caption (not an option)
        var evfactlabel = Array.from(mainbody.getElementsByTagName('label')).find(element => element.title == 'evfactbox');
        if (evfactlabel !== undefined && evfactlabel !== null) {
            evfactlabel.parentElement.parentElement.parentElement.style.display = 'none';
        }

        var maintag = mainbody.getElementsByTagName('main');
        if (maintag !== undefined && maintag !== null && maintag.length >= 1) {
            // Options
            var defwidth = '1000px';
            var hidefactbox = true;
            if (options !== undefined && options !== null) {
                var optobj = JSON.parse(options);
                // width
                if (optobj.width !== undefined && optobj.width !== null) {
                    defwidth = optobj.width;
                }

                if (optobj.hidefactbox !== undefined && optobj.hidefactbox !== null) {
                    hidefactbox = optobj.hidefactbox;
                }
            }


            // Hide BC Factbox
            if (hidefactbox === true) {
                var rightdivClass = maintag[0].getElementsByClassName('ms-nav-layout-aside-right');
                if (rightdivClass !== undefined && rightdivClass.length >= 1) {
                    rightdivClass[0].style.display = 'none';
                    // rightdivClass[0].style.flexBasis = '0';
                    // rightdivClass[0].style.visibility = 'hidden';
                }
            }

            var divClass = maintag[0].getElementsByClassName('ms-nav-layout-content');
            if (divClass !== undefined) {                
                var evfactboxdiv = document.createElement('div');
                evfactboxdiv.id = 'evfactbox';
                evfactboxdiv.style.width = defwidth;

                // pdf.js
                //                 var evpdfCanvas = document.createElement('canvas');
                //                 evpdfCanvas.id = 'evpdfcanvas';
                //                 evfactboxdiv.appendChild(evpdfCanvas);

                //                 debugger;
                //                 pdfjsLib.GlobalWorkerOptions.workerSrc = pdfjsWorker;

                //                 var loadingTask = pdfjsLib.getDocument({ data: atob(base64pdf) });
                //                 loadingTask.promise.then(function (pdf) {
                //                     console.log('PDF loaded');

                //                     // Fetch the first page
                //                     var pageNumber = 1;
                //                     pdf.getPage(pageNumber).then(function (page) {
                //                         console.log('Page loaded');

                //                         var scale = 1.5;
                //                         var viewport = page.getViewport({ scale: scale });

                //                         // Prepare canvas using PDF page dimensions
                // //                        var canvas = document.getElementById('evpdfcanvas');

                //                         var context = evpdfCanvas.getContext('2d');
                //                         evpdfCanvas.height = viewport.height;
                //                         evpdfCanvas.width = viewport.width;

                //                         // Render PDF page into canvas context
                //                         var renderContext = {
                //                             canvasContext: context,
                //                             viewport: viewport
                //                         };
                //                         var renderTask = page.render(renderContext);
                //                         renderTask.promise.then(function () {
                //                             console.log('Page rendered');
                //                         });
                //                     });
                //                 }, function (reason) {
                //                     // PDF loading error
                //                     console.error(reason);
                //                 });

                evfactboxdiv.innerHTML = '<embed height="100%" width="100%" type="application/pdf" src="data:application/pdf;base64,' + base64pdf + '">`;';
                divClass[0].appendChild(evfactboxdiv);
            } else {
                alert('main tag not found!');
            }

        } else {
            alert('Mainbody tag not found!');
        }
    } else {
        alert('No main window!');
    }

    // window.parent.document.body is the key..
    // mainElement.insertAdjacentHTML('beforeend', '<div id="two">two</div>');
    // classDiv.insertAdjacentHTML('beforeend', '<div id="two">two</div>');
}

// function initialize() {
//     function fill(frame) {
//         debugger;
//         if (!frame)
//             return;
//         frame.style.position = "fixed";
//         frame.style.width = "100vw";
//         frame.style.height = "100vh";
//         frame.style.margin = "0";
//         frame.style.border = "0";
//         frame.style.padding = "0";
//         frame.style.top = "0";
//         frame.style.left = "0";
//         frame.ownerDocument.querySelector("div.nav-bar-area-box").style.display = "none";
//         frame.ownerDocument.querySelector("div.ms-nav-layout-head").style.display = "none";
//     }

//     window.top.document.getElementById("product-menu-bar").style.display = "none";
//     fill(window.frameElement);
//     fill(window.frameElement.ownerDocument && window.frameElement.ownerDocument.defaultView && window.frameElement.ownerDocument.defaultView.frameElement);
// }    
