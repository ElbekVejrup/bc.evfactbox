controladdin "PdfViewer_EVAS"
{
    RequestedHeight = 0;
    MinimumHeight = 0;
    MaximumHeight = 0;
    RequestedWidth = 0;
    MinimumWidth = 0;
    MaximumWidth = 0;
    VerticalStretch = false;
    VerticalShrink = false;
    HorizontalStretch = false;
    HorizontalShrink = false;
    Scripts = 'Source\controladdin\Scripts\jquery.js', 'Source\controladdin\Scripts\EVFactBox.js';
    StartupScript = 'Source\controladdin\Scripts\startupScript.js';
    event ControlReady();
    procedure EVFactBox(Base64Value: Text; Options: Text)
    event Message(Value: text)
}