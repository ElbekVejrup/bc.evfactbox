pageextension 50101 "Main Test Page" extends 9308
{
    layout
    {
        addlast(Content)
        {
            part(jspart; evfactbox)
            {
                ApplicationArea = All;
            }
        }
    }
    actions
    {
        addlast(Processing)
        {
            action(DoSomething)
            {
                ApplicationArea = All;
                trigger OnAction()
                begin
                    CurrPage.jspart.Page.showEVFactBox('', '');
                end;
            }
        }
    }
}